//index_promise

const mongoose = require('mongoose');
const url = 'mongodb://localhost:27017/mygame';

mongoose.connect(url, {useNewUrlParser: true} );

const db =  mongoose.connection;
db.once('open', _ => {
    console.log('Database connected:', url);
});

db.on('error', err => {
    console.error('connection error:', err);
});

const Character = require('./models/Character');

function saveCharacter(character){
    const c = new Character(character);
    return c.save();
}

saveCharacter( { 
    name: 'Mario2',
    ultimate:  'Throw hat'
})
.then(doc => { console.log(doc); })
.catch(error => {console.error(error);});

