const mongoose = require('mongoose');
const url = 'mongodb://localhost:27017/mygame';

mongoose.connect(url, {useNewUrlParser: true} );

const db =  mongoose.connection;
db.once('open', _ => {
    console.log('Database connected:', url);
});

db.on('error', err => {
    console.error('connection error:', err);
});

const Character = require('./models/CharacterUnique');


async function workInDB() {
    await Character.deleteMany({});  // Because of Uniqueness

    
    // INSERT - CREATE
    const mario = new Character({ 
        name: 'Mario',
        ultimate:  'Throw hat'
    });

    const doc1 = await mario.save(); 
    console.log(`doc1=> ${doc1}`);
    
    const luigi = new Character({
        name: 'Luigi',
        ultimate:  'Vaccum'
    });
    const doc2 = await luigi.save();
    console.log(`doc2=> ${doc2}`);

    // QUERIES "SELECT" - READ
    const qry1 = await Character.findOne({ name: 'Mario'});
    console.log(`findOne_qry1=> ${qry1}`)

    const qry2 = await Character.findOne({});
    console.log(`findOne_qry2=> ${qry2}`);

    
    const qry3 = await Character.find({ name: 'Mario'});
    console.log(`find_qry3=> ${qry3}`)

    const qry4 = await Character.find({});
    console.log(`find_qry4=> ${qry4}`);

    // UPDATE
    const upd1 = await Character.findOneAndUpdate({ name: 'Mario'},
    {
        specials: [
          'Walk',
          'Run',
          'Jump'
        ]
    });
    console.log(`upd1=> ${upd1}`);

    // DELETE 
    const del_1 = await Character.findOne({ name: 'Mario' });
    const deleted1 = await del_1.remove();
    console.log(`del_1=> ${deleted1}`);

    const deleted2 = await Character.findOneAndDelete({ name: 'Luigi' });
    console.log(`del_2=> ${deleted2}`);
}

workInDB()
.catch(error => {console.error(error);});

